# prototype

This repository contains prototype files for testing the feasibility of a learn-integrated CI/CD native assessment grading flow for student work.

gitlabcohortusers.rb generates a cohort group for a given school and adds student users to their cohort-specific group.

gitlabrepo.rb takes a template repository and generates a fork available to a single student in a cohort.
