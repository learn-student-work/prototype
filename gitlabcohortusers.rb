require "httparty"
require "pp"

class GitlabCohortCreate
  GITLAB_API_VERSION = "v4"
  def initialize
    @origin = "gitlab.com"

    @students = [
      { username: "pgrunde", name: "peter-grunde" },
      { username: "pipeline.test", name: "pipeline-test" }
    ]
    @cohort_group = "cohort-01"
    @school_group = "learn-curriculum-test"
  end

  def read_endpoint_data(endpoint, headers)
    JSON.parse(HTTParty.get(endpoint, headers: headers).body)
  end

  def toplevel_group_id
    return @toplevel_group_id if @toplevel_group_id

    group_url = "https://#{@origin}/api/#{GITLAB_API_VERSION}/groups"
    data = read_endpoint_data(group_url, headers)
    json = data.find {|group| group["full_path"] == @school_group.to_s}

    @toplevel_group_id = json["id"]
    @toplevel_group_id
  end

  def subgroups_for_group(id)
    read_endpoint_data("https://#{@origin}/api/#{GITLAB_API_VERSION}/groups/#{id}/subgroups", headers)
  end

  def create_group(parent_id, group_name)
    group_post_url = "https://#{@origin}/api/#{GITLAB_API_VERSION}/groups"
    new_group = HTTParty.post(group_post_url,
                              headers: headers,
                              body: { parent_id: parent_id, name: group_name, path: group_name.to_s })
    new_group["id"]
  end

  def add_member_to_group(user_id, group_id)
    member_add_url = "https://#{@origin}/api/#{GITLAB_API_VERSION}/groups/#{group_id}/members"
    HTTParty.post(member_add_url,
                  headers: headers,
                  body: { invite_source: "Galvanize Learn", user_id: user_id, access_level: 30 })
  end

  def id_for_user(username)
    user_url = "https://#{@origin}/api/#{GITLAB_API_VERSION}/users?username=#{username}"
    data = read_endpoint_data(user_url, headers)
    data[0]["id"]
  end

  def generate_cohort_with_students
    subgroups = subgroups_for_group(toplevel_group_id)
    cohort_group = subgroups.find {|group| group["name"] == @cohort_group.to_s}
    cohort_group_id = cohort_group.nil? ? create_group(toplevel_group_id, @cohort_group) : cohort_group["id"]

    @students.each do |user|
      user_id = id_for_user(user[:username])
      next unless user_id

      student_group_id = create_group(cohort_group_id, user[:name])
      next unless student_group_id

      add_member_to_group(user_id, student_group_id)
    end
  end

  def headers
    { "Private-Token" => "" }
  end
end

gitlab = GitlabCohortCreate.new

gitlab.generate_cohort_with_students
