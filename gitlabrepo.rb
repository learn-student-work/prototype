require "httparty"
require "pp"

class GitlabSetup
  GITLAB_API_VERSION = "v4"
  def initialize
    @origin = "gitlab.com"
    @org = "pg-curriculum/templates"
    @repo_name = "project-pipeline-01"

    @student_group = "perry-branch"
    @cohort_group = "pg-cohort-01"
    @school_group = "pg-curriculum"

    @pipeline_template_group = "pipeline-templates"
  end

  def read_endpoint_data(endpoint, headers)
    JSON.parse(HTTParty.get(endpoint, headers: headers).body)
  end

  def gitlab_project_search_by_repo_name
    project_search_url = "https://#{@origin}/api/#{GITLAB_API_VERSION}/projects?search=#{@repo_name}&membership=true"
    data = read_endpoint_data(project_search_url, headers)
    json = data.find {|project| project["path_with_namespace"] == "#{@org}/#{@repo_name}"}
    puts "Forking from project #{json['path']}"
    json["id"]
  end

  def student_namespace_id
    namespace_search_url = "https://#{@origin}/api/#{GITLAB_API_VERSION}/namespaces?search=#{@student_group}"
    target_namespace = read_endpoint_data(namespace_search_url, headers).find do |namespace|
      namespace["full_path"] == "#{@school_group}/#{@cohort_group}/#{@student_group}"
    end
    puts "Forking into namespace #{target_namespace['full_path']}"
    target_namespace["id"]
  end

  def generate_repo_from_template
    source_id = gitlab_project_search_by_repo_name
    # TODO: check first to see if repo has already been forked and given to the student
    target_id = fork_project(source_id)
    update_student_project_settings(target_id)
  end

  def fork_project(id)
    fork_post_url = "https://#{@origin}/api/#{GITLAB_API_VERSION}/projects/#{id}/fork"
    forked_repo = HTTParty.post(fork_post_url,
                                headers: headers,
                                body: { id: id, name: @repo_name, namespace_id: student_namespace_id })
    forked_repo["id"]
  end

  def update_student_project_settings(id)
    edit_project_url = "https://#{@origin}/api/#{GITLAB_API_VERSION}/projects/#{id}"
    update_response = HTTParty.put(edit_project_url,
                                   headers: headers,
                                   body: {
                                     ci_config_path: ".gitlab-ci.yml@#{@school_group}/#{@pipeline_template_group}/hello-world", # TODO: how to determine automated location of .gitlab-ci.yml file?
                                       allow_merge_on_skipped_pipeline: false,
                                       forking_access_level: "disabled",
                                       only_allow_merge_if_pipeline_succeeds: true,
                                       approvals_before_merge: 1 # is this sufficient to prevent merging or do we need the rules?
                                   })
  end

  private

  # token scope here needs full api read / write
  def headers
    { "Private-Token" => "" }
  end
end

setup = GitlabSetup.new
setup.generate_repo_from_template
